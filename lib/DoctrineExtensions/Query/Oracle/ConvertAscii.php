<?php

namespace DoctrineExtensions\Query\Oracle;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * "CONVERTASCII" "(" StringPrimary ")"
 */
class ConvertAscii extends FunctionNode
{
    protected $stringPrimary = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->stringPrimary = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'CONVERT(LOWER(' .
            $sqlWalker->walkSimpleArithmeticExpression($this->stringPrimary) . '), ' .
            "'us7ascii', " .
            "'utf8'" .
        ')';
    }
}